import 'package:flutter/material.dart';

const kBottomContainerHeight = 80.0;
final kBottomContainerColor = Color(0xFFEB1555);
final kActiveCardColor = Color(0xFF1D1E33);
final kInactiveCardColor = Color(0xFF111328);

final kLabelTextStyle = TextStyle(
  fontSize: 18.0,
  color: Color(0xFF8D8E98),
);

final kNumberTextStyle = TextStyle(
  fontSize: 50,
  fontWeight: FontWeight.w900,
);

final kLargeButtonTextStyle = TextStyle(
  fontSize: 25,
  fontWeight: FontWeight.bold,
);

final kTitleTextStyle = TextStyle(
  fontSize: 50,
  fontWeight: FontWeight.bold,
);

final kResultTextStyle = TextStyle(
  fontSize: 22,
  color: Color(0xFF24D876),
  fontWeight: FontWeight.bold,
);

final kBMITextStyle = TextStyle(
  fontSize: 100,
  fontWeight: FontWeight.bold,
);

final kBodyTextStyle = TextStyle(
  fontSize: 22,
);
